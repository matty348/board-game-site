## Dependencies

This site runs on Django 1.7 and uses Jinja2 and [django-jinga](http://niwibe.github.io/django-jinja/) for templating.

Run `pip install -r requirements.txt` from the project root to install dependencies.

## Starting The Server

Assuming you have everything set up on your machine to run the site, the following command will get it up and running.

`python manage.py runserver localhost:8080`

## Local Settings

Local settings such as database connection info should be configured in a `settings_local.py` file in the `boardgame` directory. To create one, copy the 'settings_local.py.txt' file and remove the `.txt` extention. Make any setting overrides you need in this file. It is gitignored so your personal settings will not get added to the repo.
