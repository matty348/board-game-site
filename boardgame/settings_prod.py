# Production Settings

from __future__ import absolute_import

from os import environ

DATABASES = {
  'default': {
  'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
    'NAME': 'boardgames',           # Or path to database file if using sqlite3.
    'USER': 'djangouser',           # Not used with sqlite3.
    'PASSWORD': 'agritan2012',          # Not used with sqlite3.
    'HOST': '',           # Set to empty string for localhost. Not used with sqlite3.
    'PORT': '',           # Set to empty string for default. Not used with sqlite3.
  }
}

FORCE_SCRIPT_NAME = '/boardgame'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/home/matty348/webapps/static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = 'http://matty348.webfactional.com/static/'
ADMIN_MEDIA_PREFIX = '/static/admin/'