#!/usr/local/bin/python2.7
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "boardgame.settings")

from django.conf import settings
import django
from gamestats.models import Game
django.setup()

import urllib2, urllib, xml.etree.ElementTree as ET, socket
import boardgame.settings
#loops over all games
for game in Game.objects.all():
	print game.bgg_id
	response = urllib2.urlopen(r'http://www.boardgamegeek.com/xmlapi2/thing?stats=1&id=' + str(game.bgg_id))
	root = ET.fromstring(response.read())
	thumbnail = "http://"+root[0].find("thumbnail").text.split("//")[1]
	image = "http://"+root[0].find("image").text.split("//")[1]
	print thumbnail
	print image
	if socket.gethostname() != 'web352.webfaction.com':
		urllib.urlretrieve(thumbnail, filename="/home/matt/board-game-site/gamestats/static/media/bg_thumbs/"+str(game.bgg_id)+".jpg")
		urllib.urlretrieve(image, filename="/home/matt/board-game-site/gamestats/static/media/bg_images/"+str(game.bgg_id)+".jpg")
	else:
		urllib.urlretrieve(thumbnail, filename="/home/matty348/webapps/static/media/bg_thumbs/"+str(game.bgg_id)+".jpg")
		urllib.urlretrieve(image, filename="/home/matty348/webapps/static/media/bg_images/"+str(game.bgg_id)+".jpg")
