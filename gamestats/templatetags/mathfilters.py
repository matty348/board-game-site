from django import template
from django_jinja import library
register = template.Library()


@library.filter
def divided_percent(value1, value2):
    if value1 != 0 and value2 != 0:
        percent = round(value1/float(value2)*100, 2)
        return str(percent)+"%"
    else:
        return "0%"