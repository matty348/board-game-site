from django import template
from django_jinja import library
register = template.Library()


@library.filter
def duration_format(value):
	value = int(value)
	d = 'day'
	h = 'hour'
	m = 'minute'
	days = int(value/1440)
	hours = int(value%1440/60)
	minutes = value%60
	if days != 1:
		d += 's'
	if hours != 1:
		h += 's'
	if minutes != 1:
		m += 's'
	toReturn = ""
	if days != 0:
		toReturn += str(days)+" "+d+" "
	if hours != 0:
		toReturn += str(hours)+" "+h+" "
	if minutes !=0:
		toReturn += str(minutes)+" "+m
	return toReturn