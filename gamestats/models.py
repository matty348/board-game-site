from django.db import models
from django.contrib.auth.models import User
import datetime 



# Create your models here.
class Mechanic(models.Model):
  name = models.CharField(max_length=250, unique=True)
  description = models.TextField()

  def __str__(self): 

    return self.name 

  class Meta: 

    ordering = ['name'] 

  class Admin: 

    pass

class Stat(models.Model):
  name = models.CharField(max_length=100, unique=True)
  description = models.TextField()
  values = models.ManyToManyField('Value', null=True, blank=True)

  def __str__(self): 
    return self.name 

  class Meta: 
    ordering = ['name'] 

  class Admin: 
    pass

class Value(models.Model):
  name = models.CharField(max_length=100, unique=True)
  description = models.TextField()

  def __str__(self): 
    return self.name 

  class Meta: 
    ordering = ['name'] 

  class Admin: 
    pass

class Category(models.Model):
  name = models.CharField(max_length=255)
  description = models.TextField()
  
  class Meta: 
    ordering = ['name'] 
    
  def __str__(self): 
    return self.name

  class Admin: 
    pass

class Game(models.Model): 
  bgg_id = models.IntegerField()
  name = models.CharField(max_length=250, unique=True) 
  min_players = models.IntegerField(default=2)
  max_players = models.IntegerField(default=2)
  bgg_playing_time = models.IntegerField(default=2)
  bgg_bayesaverage = models.DecimalField(max_digits=3, decimal_places=2)
  description = models.TextField()
  image = models.CharField(max_length=250, blank=True, null=True)
  weight = models.DecimalField(max_digits=5, decimal_places=4)
  added = models.DateTimeField(default=datetime.datetime.now) 
  expands = models.ForeignKey('self', null=True, blank=True, default=None, related_name="Expansions")
  categories = models.ManyToManyField(Category)
  mechanics = models.ManyToManyField(Mechanic)
  owners = models.ManyToManyField(User)
  Playerstats = models.ManyToManyField(Stat, null=True, blank=True)
  Gamestats = models.ManyToManyField(Stat, null=True, blank=True, related_name="Gamestats")
  
  def get_avg_playtime(self):
    plays = Play.objects.filter(game=self)
    count = len(plays)
    sum = 0
    for play in plays:
      sum += play.play_time
    if count > 0:
      return (sum/count)
    else:
		return (0)

  def __str__(self): 

    return self.name 

    #Overriding
  def save(self, *args, **kwargs):
    import urllib2, urllib, xml.etree.ElementTree as ET, socket
    response = urllib2.urlopen(r'http://www.boardgamegeek.com/xmlapi2/thing?stats=1&id=' + str(self.bgg_id))
    root = ET.fromstring(response.read())
    thumbnail = "http://"+root[0].find("thumbnail").text.split("//")[1]
    image = "http://"+root[0].find("image").text.split("//")[1]
    if socket.gethostname() != 'web352.webfaction.com':
      urllib.urlretrieve(thumbnail, filename="/home/matt/board-game-site/gamestats/static/media/bg_thumbs/"+str(self.bgg_id)+".jpg")
      urllib.urlretrieve(image, filename="/home/matt/board-game-site/gamestats/static/media/bg_images/"+str(self.bgg_id)+".jpg")
    else:
      urllib.urlretrieve(thumbnail, filename="/home/matty348/webapps/static/media/bg_thumbs/"+str(self.bgg_id)+".jpg")
      urllib.urlretrieve(image, filename="/home/matty348/webapps/static/media/bg_images/"+str(self.bgg_id)+".jpg")
    super(Game, self).save(*args, **kwargs)

  class Meta: 

    ordering = ['name'] 

  class Admin: 

    pass


class Play(models.Model):
  game = models.ForeignKey(Game)
  play_image = models.FileField(null=True, blank=True, upload_to='play_images/%Y/%m/%d')
  date = models.DateField(default=datetime.date.today)
  play_time = models.IntegerField()
  num_players = models.IntegerField()
  use_for_stats = models.BooleanField()
  expansions = models.ManyToManyField(Game, null=True, blank=True, related_name="Play Expansions")
  
  def __str__(self): 
    return str(self.id)

  class Admin: 
    pass

class Playstat(models.Model):
  stat = models.ForeignKey(Stat)
  play = models.ForeignKey(Play)
  player = models.ForeignKey(User)
  value = models.CharField(max_length=255)
    
  def __str__(self): 
    return str(self.id)

  class Admin: 
    pass
  


