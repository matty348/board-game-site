from django.contrib import admin
from gamestats.models import *
from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms import ModelForm

admin.site.register(Mechanic)
admin.site.register(Stat)
admin.site.register(Value)

admin.site.register(Category)
admin.site.register(Playstat)

class PlaystatInline(admin.TabularInline):
    model = Playstat
    extra = 3

class PlayAdmin(admin.ModelAdmin):
    inlines = [PlaystatInline]
    class Media:
      js = ("javascript/play.js",)
    

admin.site.register(Play, PlayAdmin)


class GameAdmin(admin.ModelAdmin):
    model = Game
    filter_horizontal = ('categories','mechanics', 'Playerstats', 'Gamestats',)
    class Media:
      js = ("javascript/game.js",)

admin.site.register(Game, GameAdmin)