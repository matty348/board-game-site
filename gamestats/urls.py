from django.conf.urls import patterns, url, include
from gamestats.views import general_views, player_views, json_views
from django.views.static import * 
from django.conf import settings

player_patterns = [
	url(r'^$', player_views.player, name='player'),
	url(r'^plays$', player_views.player_plays, name='player_plays'),
	url(r'^wins$', player_views.player_wins, name='player_wins'),
	url(r'^owned$', player_views.player_owned, name='player_owned'),
	url(r'^mechanics$', player_views.player_mechanics, name='player_mechanics'),
	url(r'^mechanics/(?P<mechanic_id>\d+)/wins/$', player_views.player_mechanics_game_wins,
		 name='player_mechanics_game_wins'),
	url(r'^mechanics/(?P<mechanic_id>\d+)/plays/$', player_views.player_mechanics_game_plays,
		 name='player_mechanics_game_plays'),
	url(r'^categories$', player_views.player_categories, name='player_categories'),
	url(r'^categories/(?P<category_id>\d+)/wins/$', player_views.player_categories_game_wins,
		 name='player_categories_game_wins'),
	url(r'^categories/(?P<category_id>\d+)/plays/$', player_views.player_categories_game_plays,
		 name='player_categories_game_plays'),
	url(r'^played-games$', player_views.player_played_games, name='player_played_games'),
]

urlpatterns = patterns('',
	url(r'^bggapi/(?P<game_id>\d+)/$', general_views.bggapi, name='bggapi'),
	url(r'^game/(?P<game_id>\d+)/$', general_views.game, name='game'),
	url(r'^player/(?P<player_id>\d+)/', include(player_patterns)),
	url(r'^play/(?P<play_id>\d+)/$', general_views.play, name='play'),
	url(r'^play/add/$', general_views.add_play, name='add_play'),
	url(r'^main/$', general_views.main, name='main'),
	url(r'^all/(?P<content_type>\w+)/$', general_views.all_content, name='all_content'),
  url(r'^api/play_stats/(?P<metric>\w+)/$', json_views.play_stats_by_month, name='play_stats_by_month'),
)

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()