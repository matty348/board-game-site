/** 
 * Graphs generated with d3.js
 */

/** 
 * Plays by month line graph
 */
if ($('#plays-by-month').length) {
  var metric = "plays",
      $container = $('#plays-by-month'),
      margin = {
        top: 20, 
        right: 20, 
        bottom: 30, 
        left: 50
      },
      width = $container.width() - margin.left - margin.right,
      height = $container.width()/2.6 - margin.top - margin.bottom,
      barWidth = width/12;


  playsGraph(metric);
  
  $(window).resize( function() {
    playsGraph(metric);
  });

  // Enable toggles to switch the metric
  $('.data-toggle').click( function(e) {
    var newMetric = $(this).data('metric');
    if (newMetric != metric) {
      shiftPeriod('reset');
      playsGraph(newMetric);
      metric = newMetric;
    }
    $(this).addClass('selected').siblings().removeClass('selected');
    e.preventDefault();
  })
}
  
/** 
 * Generate a bar graph of play data by month.
 * @param {string} metric one of plays, time, or players
 */
function playsGraph(metric) {

  $container.find('svg').empty();

  var parseDate = d3.time.format("%Y-%m-%dT00:00:00Z").parse;
  var monthName = d3.time.format("%b %y");

  var y = d3.scale.linear()
    .range([height, 0]);

  var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

  var svg = d3.select('#plays-by-month svg')
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  // Setting up a clipping rectangle to contain the bar graph.
  var clipping = svg.append("defs")
    .append("clipPath")
    .attr("id", "clip-bars")
    .append("rect")
    .attr("width", width)
    .attr("height", height + margin.bottom);

  // Setting up container for bars.
  var bars = svg.append("g")
    .attr('id', 'bars-container')
    .attr("clip-path", "url(#clip-bars)")
    .append("g")
    .attr('id', 'bars');

  d3.json(window.bgs.json.playStats[metric], function(error, data) {
    data.forEach(function(d) {
      d.month = parseDate(d.month);
      d.stat = +d.stat;
      if (metric == "time") {
        metric = "minutes";
      }
    });

    var maxCount = d3.max(data, function(d) { return d.stat; }),
        numRecords = data.length,
        barHeight = function(d) { return d.stat * (height / maxCount); },
        barsOffset = -(numRecords - 12) * barWidth;

    bars.attr("width", barWidth * numRecords)
      .attr("transform", "translate(" + barsOffset + " , 0)")
      .attr("data-bar-width", barWidth)
      .attr("data-bars-offset", barsOffset)
      .attr("data-orig-offset", barsOffset)
      .attr("data-num-records", numRecords);

    y.domain([0, maxCount]);

    var bar = bars.selectAll("g")
        .append("g")
        .data(data)
        .enter().append("g")
        .attr("transform", function(d, i) {
          return "translate(" + barWidth * i + ", " + (height - barHeight(d)) + ")"; 
        });

    bar.append("rect")
      .attr("width", barWidth - 2)
      .attr("height", barHeight)
      .attr("class", function(d) { return "count-" + thresholdString(d.stat, maxCount);});

    bar.append("text")
      .attr("x", barWidth / 2)
      .attr("y", 14)
      .attr("text-anchor", "middle")
      .attr("dy", ".35em")
      .text(function(d) { return d.stat; });

    bar.append("text")
      .attr("x", barWidth / 2)
      .attr("y", function(d) { return d.stat * (height / maxCount) + 12; })
      .attr("text-anchor", "middle")
      .attr("dy", ".35em")
      .text(function(d) { return monthName(d.month); });
    
    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text(metric);
  });
}

// Move between time periods
var $next = $('.next-period'),
    $prev = $('.prev-period');

$prev.add($next).click(function(e) {
  shiftPeriod($(this).data('direction'));
  e.preventDefault();
});

/** 
 * Shift the bar graph to reveal a different period of time
 * @param  {string} dir   direction in which the graph should move, either
 *                        'left', 'right' or 'reset'
 */
function shiftPeriod(dir) {

  var bars = d3.select('#bars'),
      offset = parseInt(bars.attr('data-bars-offset')),
      origOffset = parseInt(bars.attr('data-orig-offset')),
      numRecords = parseInt(bars.attr('data-num-records')),
      newOffset = 0;

  if (dir == 'left') {
    newOffset = offset + (barWidth * 12);
    newOffset = newOffset > 0 ? 0 : newOffset;
  } else if (dir == 'right') {
    newOffset = offset - (barWidth * 12);
    newOffset = newOffset < origOffset ? origOffset : newOffset;
  } else {
    newOffset = origOffset;
  }

  if ( (dir == 'left' && offset < 0) || (dir =='right' && offset > origOffset) || (dir == "reset" && offset != origOffset)) { 
    bars.transition()
      .duration(500)
      .attr("transform", "translate(" + newOffset + " , 0)")
      .attr('data-bars-offset', newOffset);
  } 

  // hide prev link when we run out of bars
  if (newOffset >= 0) {
    $prev.fadeOut();
  } else {
    $prev.fadeIn();
  }

  // hide next link when we get to proginal position
  if ( newOffset == origOffset) {
    $next.fadeOut();
  } else {
    $next.fadeIn();
  }

}

/**
 * Generate the string 'low', 'medium', or 'high' based on a value and a range 
 * @param  {int} value - the value to check against
 * @param  {int} range - the upper limit of the range from 0
 * @return {string} - 'low', 'medium', or 'high'
 */
function thresholdString(value, range) {
  var c = '',
      s = value/range;
  if (s >= 2/3) {
    c = 'high';
  } else if (s < 2/3 && s >= 1/3) {
    c = 'medium';
  } else if (s < 1/3) {
    c = 'low';
  }
  return c;
}

