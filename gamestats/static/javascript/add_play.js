$(document).ready(function(){
	$('#save').attr('disabled', 'disabled');
	var button = $("<button>Get Playerstats</button>");
	button.click(function() {
		get_forms();
		return false;
	});
	button.appendTo("#play");
});



function get_forms(){
	var game = $("#id_game").val();
	var num_players = $("#id_num_players").val();
	$.ajax({type:"POST", data:$("#form").serialize(), url:"../../playstat_forms/"+game+"/"+num_players+"/",success:function(result){
		handle_forms(result);
		$('.add_field').click(function(){
			add_field(this);
		});
	}});
}

function handle_forms(forms){
	$('#stats').empty();
	$('#stats').append(forms);
	$('.players').change(function(){
		var row = $(this).attr('id');
		var val = $('#'+row).val();
		$('.'+row + ' div input:nth-child(2)').val(val);
	});
	$('#save').removeAttr('disabled');
	return false;
}


function add_field(element){
	var new_field = $(element).parent().first().children(":first").html();
	new_field = new_field.replace(/id_\d*/g, "id_"+next_form_num);
	new_field = new_field.replace(/name="\d*/g, "name=\""+next_form_num);
	new_field = $("<div>").append(new_field);
	$(element).before(new_field);
	$("total_forms").val(next_form_num);
	next_form_num += 1;
}