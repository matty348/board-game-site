var $ = django.jQuery;
$(document).ready(function(){
  $("#id_bgg_id").parent().append('<input type="button" value="Check BGG">').click(function(){ updateBGG();});
});



function updateBGG(){
  var val = $("#id_bgg_id").val();
  $.ajax({url:"../../../../gamestats/bggapi/"+val,success:function(result){
    var name = $(result).find('name').attr("value");
    var minPlayers = $(result).find('minplayers').attr("value");
    var maxPlayers = $(result).find('maxplayers').attr("value");
    var description = $(result).find('description').text().replace(/&#10;/g, " ");
    var playingTime = $(result).find('playingtime').attr("value");
	var bayesaverage  = Math.round($(result).find('bayesaverage').attr("value")*100)/100;
	var averageweight   = Math.round($(result).find('averageweight').attr("value")*100)/100;
    var image  = $(result).find('image').text();
    $("#id_name").val(name);
    $("#id_min_players").val(minPlayers);
    $("#id_max_players").val(maxPlayers);
    $("#id_bgg_playing_time").val(playingTime);
    $("#id_bgg_bayesaverage").val(bayesaverage);
    $("#id_description").text(unescape(description));
    $("#id_weight").val(averageweight);
    $("#id_image").val(image);
    
    //TODO preselect mechanics
  }});
}