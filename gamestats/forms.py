from django import forms
from django.forms import ModelForm
from gamestats.models import Play, Playstat, Game
from django.contrib.auth.models import User

class play_form(ModelForm):
	class Meta:
		model = Play
		
class playstat_form(ModelForm):
	class Meta:
		model = Playstat
		exclude = ('play',)
		
class play_info_form(forms.Form):
	game = forms.ModelChoiceField(queryset=Game.objects.all())
	players = forms.ModelMultipleChoiceField(queryset=User.objects.all())
	expansions = forms.ModelMultipleChoiceField(queryset=Game.objects.filter(expands__isnull = False))