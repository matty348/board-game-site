from django.template import RequestContext, loader
from django.http import HttpResponse
from gamestats.models import Play, Mechanic, Category


### Helper Functions////////////////////////

def render_page(variables, template_name_list, request):
	#loading template names as a list allows for fallback templates if the first
	#in the list does not exist this is especially useful for games where they may
	#or may not have a custom page
	template = loader.select_template(template_name_list)
	context = RequestContext(request, variables)
	return HttpResponse(template.render(context))

def get_mechanic_wins_list(user):
	mechanic_wins_list = []
	for mechanic in Mechanic.objects.all():
		mechanic_wins = len(Play.objects.filter(playstat__player=user, playstat__value="T", playstat__stat__name="won", game__mechanics=mechanic))
		mechanic_plays = len(Play.objects.filter(playstat__player=user, playstat__stat__name="won", game__mechanics=mechanic))
		if mechanic_plays > 0:
				mechanic_wins_list.append({'name': mechanic.name, 'id': mechanic.id, 'wins': mechanic_wins, 'plays': mechanic_plays})
	return mechanic_wins_list

def get_category_wins_list(user):
	category_wins_list = []
	for category in Category.objects.all():
		category_wins = len(Play.objects.filter(playstat__player=user, playstat__value="T", playstat__stat__name="won", game__categories=category))
		category_plays = len(Play.objects.filter(playstat__player=user, playstat__stat__name="won", game__categories=category))
		if category_plays > 0:
				category_wins_list.append({'name': category.name, 'id': category.id, 'wins': category_wins, 'plays': category_plays})
	return category_wins_list
