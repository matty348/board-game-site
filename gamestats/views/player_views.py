from django.contrib.auth.models import User
from gamestats.models import Game, Play, Playstat, Stat, Mechanic, Category
from django.db.models import Avg, Count
from gamestats.views.helpers import render_page, get_mechanic_wins_list, get_category_wins_list


def player_plays(request, player_id):
	player = User.objects.get(pk=player_id)
	plays = Play.objects.filter(playstat__player=player).distinct().order_by("-date")
	variables = {
		'title': "All Plays for "+player.username,
		'plays':plays,
		'player': player,
		}
	return render_page(variables, ['play_list.jinja'], request)


def player_wins(request, player_id):
	player = User.objects.get(pk=player_id)
	wins = Play.objects.filter(playstat__player=player, playstat__value="T", playstat__stat__name="won").distinct().order_by("-date")
	variables = {
		'title': "All Wins for "+player.username,
		'plays':wins,
		'player': player,
		}
	return render_page(variables, ['play_list.jinja'], request)


def player_owned(request, player_id):
	player = User.objects.get(pk=player_id)
	variables = {
			'title': "All owned Games for "+player.username,
			'games':Game.objects.filter(owners__in=[player]),
			'player': player,
			}
	return render_page(variables, ['game_list.jinja'], request)


def player_mechanics(request, player_id):
	player = User.objects.get(pk=player_id)
	mechanic_wins_list = get_mechanic_wins_list(player)
	variables = {
		'title': "All Mechanics Played for "+player.username,
		'mechanics': mechanic_wins_list,
		'player': player,
		}
	return render_page(variables, ['player_categories/mechanics.jinja'], request)


def player_mechanics_game_wins(request, player_id, mechanic_id):
	player = User.objects.get(pk=player_id)
	mechanic = Mechanic.objects.get(pk=mechanic_id)
	wins_list = Play.objects.filter(playstat__player=player, playstat__value="T", playstat__stat__name="won", game__mechanics=mechanic)
	variables = {
		'title': "All "+mechanic.name+" wins for "+player.username,
		'plays': wins_list,
		'player': player,
		}
	return render_page(variables, ['play_list.jinja'], request)

def player_mechanics_game_plays(request, player_id, mechanic_id):
	player = User.objects.get(pk=player_id)
	mechanic = Mechanic.objects.get(pk=mechanic_id)
	wins_list = Play.objects.filter(playstat__player=player, playstat__stat__name="won", game__mechanics=mechanic)
	variables = {
		'title': "All "+mechanic.name+" plays for "+player.username,
		'plays': wins_list,
		'player': player,
		}
	return render_page(variables, ['play_list.jinja'], request)


def player_categories(request, player_id):
	player = User.objects.get(pk=player_id)
	category_wins_list = get_category_wins_list(player)
	variables = {
		'title': "All Categories Played for "+player.username,
		'categories': category_wins_list,
		'player': player,
		}
	return render_page(variables, ['player_categories/categories.jinja'], request)

def player_categories_game_wins(request, player_id, category_id):
	player = User.objects.get(pk=player_id)
	category = Category.objects.get(pk=category_id)
	wins_list = Play.objects.filter(playstat__player=player, playstat__value="T", playstat__stat__name="won", game__categories=category)
	variables = {
		'title': "All "+category.name+" wins for "+player.username,
		'plays': wins_list,
		'player': player,
		}
	return render_page(variables, ['play_list.jinja'], request)

def player_categories_game_plays(request, player_id, category_id):
	player = User.objects.get(pk=player_id)
	category = Category.objects.get(pk=category_id)
	wins_list = Play.objects.filter(playstat__player=player, playstat__stat__name="won", game__categories=category)
	variables = {
		'title': "All "+category.name+" plays for "+player.username,
		'plays': wins_list,
		'player': player,
		}
	return render_page(variables, ['play_list.jinja'], request)


def player_played_games(request, player_id):
	player = User.objects.get(pk=player_id)
	not_played = Game.objects.all().exclude(pk__in = Game.objects.filter(play__playstat__player=player).distinct())
	played = Game.objects.filter(play__playstat__player=player).distinct()
	variables = {
		'title': "All played Games for "+player.username,
		'games': played,
		'player': player,
		}
	return render_page(variables, ['game_list.jinja'], request)


def player(request, player_id):
	player = User.objects.get(pk=player_id)
	template_names = ['player.jinja']
	plays = []
	wins = Play.objects.filter(playstat__player=player, playstat__value="T", playstat__stat__name="won").order_by("-date")
	gamePoints = 0
	for win in wins:
		gamePoints += win.game.weight
	time = 0
	for play in Playstat.objects.filter(player=player).values("play").distinct().order_by("-play__date"):
		currPlay = Play.objects.get(pk=play["play"])
		plays.append(currPlay)
		time += currPlay.play_time
	mechanic_wins_list = get_mechanic_wins_list(player)
	category_wins_list = get_category_wins_list(player)
	variables = {
		'player': player,
		'win_num':len(wins),
		'wins': wins[:10],
		'plays_num': len(plays),
		'time': time,
		'games_played': len(Game.objects.filter(play__playstat__player=player).distinct()),
		'games_owned': len(Game.objects.filter(owners__in=[player])),
		'avg_game_weight': Game.objects.filter(play__playstat__player=player).aggregate(Avg('weight')),
		'points': gamePoints,
		'plays': plays[:10],
		'mechanic_wins_list': sorted(mechanic_wins_list, key=lambda k: -float(k['wins'])/k["plays"])[:10],
		'category_wins_list': sorted(category_wins_list, key=lambda k: -float(k['wins'])/k["plays"])[:10],
		'percent_wins': round(len(wins)/float(len(plays))*100, 2),
		'most_played_game': Game.objects.filter(play__playstat__player=player).annotate(
			c=Count('play',distinct=True)).order_by('-c')[0]
		}
	return render_page(variables, template_names, request)