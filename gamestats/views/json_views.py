from gamestats.models import Game, Play, Playstat, Stat, Mechanic, Category
from django.db.models import Q,Avg,Count,Sum
from django.db import connections
from django.http import JsonResponse
from gamestats.views.helpers import render_page, get_mechanic_wins_list, get_category_wins_list

def play_stats_by_month(request, metric):
  data = Play.objects.all().order_by('month')
  if metric == 'plays':
    data = Play.objects.all().order_by('month') \
        .extra(select={'month': connections[Play.objects.db].ops.date_trunc_sql('month', 'date')}) \
        .values('month') \
        .annotate(stat=Count('id'))
  elif metric == 'time':
    data = Play.objects.all().order_by('month') \
        .extra(select={'month': connections[Play.objects.db].ops.date_trunc_sql('month', 'date')}) \
        .values('month') \
        .annotate(stat=Sum('play_time'))
  elif metric == 'players':
    data = Play.objects.all().order_by('month') \
        .extra(select={'month': connections[Play.objects.db].ops.date_trunc_sql('month', 'date')}) \
        .values('month') \
        .annotate(stat=Avg('num_players'))
  else:
    data = Play.objects.all().order_by('month') \
        .extra(select={'month': connections[Play.objects.db].ops.date_trunc_sql('month', 'date')}) \
        .values('month') \
        .annotate(plays=Count('id'), time=Sum('play_time'), players=Avg('num_players'))
  return JsonResponse(list(data), safe=False)
