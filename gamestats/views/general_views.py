from django.http import HttpResponseRedirect, HttpResponse
import urllib
import urllib2
import datetime
import os
from gamestats.models import Game, Play, Playstat, Stat, Mechanic, Category
from django.template import Context, RequestContext, Template
from django.shortcuts import  redirect, get_object_or_404 #render_to_response,
from django.core.urlresolvers import reverse
from gamestats.forms import play_form, playstat_form, play_info_form
from django.forms.models import inlineformset_factory
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User
from jinja2 import FileSystemLoader, Environment
from django.conf import settings
from django.db.models import Q,Avg,Count,Sum
from django.db import connections
from django.shortcuts import render
from django import forms
from gamestats.views.helpers import render_page, get_mechanic_wins_list, get_category_wins_list

# constants
GAME_PLAYER_ID = 2#the id of the player for recording game stats this id set in fixtures.


# VIEWS
#######################################
def bggapi(request, game_id):
	id = game_id
	response = urllib2.urlopen(r'http://www.boardgamegeek.com/xmlapi2/thing?stats=1&id=' + id)
	return HttpResponse(content=response.read(), content_type='application/xml')

#TODO stats for each game
def game(request, game_id):
	game = Game.objects.get(pk=game_id)
	plays = Play.objects.filter(Q(game=game) | Q(expansions__id = game.id))
	avg_playtime =plays.aggregate(Avg('play_time'))
	players_who_have_played = User.objects.filter(id__in=Playstat.objects.filter(play__in=Play.objects.filter(game=game)).values("player"))
	template_names = ['games/' + game_id + '.jinja', 'game_base.jinja']
	variables = {
		'game':game,
		'plays':plays,
		'players_who_have_played':players_who_have_played,
		'avg_playtime':avg_playtime
		}
	return render_page(variables, template_names, request)

def all_content(request, content_type):
	if content_type == 'games':
		content = Game.objects.order_by('name')
	elif content_type == 'plays':
		content = Play.objects.all().order_by('-date')
	elif content_type == 'players':
		content = User.objects.all().order_by('username')
	template_names = ['all.jinja']
	variables = {
		'content_type': content_type,
		'content': content,
		'plays': Play.objects.all(),
	}
	return render_page(variables, template_names, request)


def play(request, play_id):
	play = Play.objects.get(pk=play_id)
	gamestats = Playstat.objects.filter(play=play, player__id = GAME_PLAYER_ID)
	playerstats = Playstat.objects.filter(Q(play=play) & ~Q(player__id = GAME_PLAYER_ID))
	players = User.objects.filter(id__in=playerstats.values('player_id'))
	winner_id = Playstat.objects.filter(play=play, stat__name="won", value="T")[0]
	
	template_names = ['play.jinja']
	variables = {
		'play': play,
		'players':players.order_by('username'),
		'playerstats':playerstats,
		'gamestats':gamestats,
		'winner':winner_id,
	}
	return render_page(variables, template_names, request)

def main(request):
	games = Game.objects.order_by('name')
	plays = Play.objects.order_by('-date')
	players = User.objects.order_by('username').exclude(username="game")
	playstats = plays[0].playstat_set.filter(stat__name='Won')
	avg_playtime = Play.objects.all().aggregate(Avg('play_time'))
	template_names = ['main.jinja']
	variables = {
		'games': games,
		'plays': plays,
		'playstats': playstats,
		'players': players,
		'avg_playtime': avg_playtime,
	}
	return render_page(variables, template_names, request)
	
@staff_member_required
@login_required
def add_play(request):
	#show initial form to get number of players and game so we know how many playstats to have
	if request.method == 'GET':
		form = play_info_form()
		variables = {'play_form': form}
		template_names = ['add_play_1.jinja']
		return render_page(variables, template_names, request)
	
	#means we came from the  initial form. Build and show second form that gets game data
	else:
		players = request.POST.getlist("players")
		game_id = request.POST.get("game")
		print game_id
		playerstats = Game.objects.get(id=game_id).Playerstats.all()
		gamestats = Game.objects.get(id=game_id).Gamestats.all()
		num_stats = len(players) * len(playerstats)
		#an object for inline forms in this case playstat forms
		playstat_formset = inlineformset_factory(Play, Playstat, extra = num_stats, can_delete=False)
		game_formset = inlineformset_factory(Play, Playstat, extra = len(gamestats), can_delete=False)

		
		#set defaults that we just asked for
		form = play_form(initial={'num_players': len(players), 'game': game_id, 'expansions': request.POST.getlist("expansions")}) 
		form.fields['num_players'].widget.attrs['readonly'] = "True"
		form.fields['game'].widget.attrs['readonly'] = "True"
		form.fields['expansions'].widget.attrs['readonly'] = "True"
		
		game_values = []
		for stat in gamestats:
			game_values.append({"stat":stat.id,"player": "2"})
			
		gamestat_forms = game_formset(initial=game_values, prefix='game')
		for f in gamestat_forms:
			f.fields['player'].widget.attrs['hidden'] = "True"
			f.fields['stat'].widget.attrs['hidden'] = "True"
			val_type = Stat.objects.get(id=f.initial["stat"]).values.first().name
			if val_type == "boolean":
				f.fields['value'] = forms.ChoiceField(widget=forms.Select, choices=(('F', 'False',), ('T', 'True',)))
			elif val_type == "#":
				f.fields['value'] = forms.ChoiceField(widget=forms.NumberInput)
			else:
				f.fields['value'] = forms.ModelChoiceField(queryset=Stat.objects.get
					(id=f.initial["stat"]).values.all(), to_field_name="name")
			
		values = []	#default values for playerstats
		for stat in playerstats:
			for player in players:
				values.append({"stat":stat.id,"player": player})
				
		playstat_forms = playstat_formset(initial=values, prefix='player')
		
		for count,f in enumerate(playstat_forms):
			f.fields['player'].widget.attrs['hidden'] = "True"
			f.fields['stat'].widget.attrs['hidden'] = "True"
			val_type = Stat.objects.get(id=values[count]["stat"]).values.first().name
			#change value fields based on content
			if val_type == "boolean":
				f.fields['value'] = forms.ChoiceField(widget=forms.Select, choices=(('F', 'False',), ('T', 'True',)))
			elif val_type == "#":
				f.fields['value'] = forms.ChoiceField(widget=forms.NumberInput)
			else:
				f.fields['value'] = forms.ModelChoiceField(queryset=Stat.objects.get(id=values[count]["stat"]).values.all(), to_field_name="name")
		if 'first' in request.POST:		
			template_names = ['add_play_2.jinja']
			variables = {
				'play_form': form,
				'playstat_forms': playstat_forms, 
				'game_form': gamestat_forms,
				'players': players,
				'playerstats': playerstats, 
				'gamestats': gamestats,
				'User': User}
			return render_page(variables, template_names, request)
		else:
			form = play_form(request.POST or None, request.FILES or None)
			if form.is_valid():
				gameformset = game_formset(request.POST, prefix="game")
				playerformset = playstat_formset(request.POST, prefix="player")
				if all([psf.is_valid() for psf in gameformset]) and all([psf.is_valid() for psf in gameformset]):
					new_play = form.save()
					for psf in gameformset:
						new_playstat = psf.save(commit=False)
						new_playstat.play = new_play
						new_playstat.save()
					for psf in playerformset:
						new_playstat = psf.save(commit=False)
						new_playstat.play = new_play
						new_playstat.save()
			else:
				template_names = ['add_play_2.jinja']
				variables = {
					'play_form': form,
					'playstat_forms': playstat_forms, 
					'game_form': gamestat_forms
					}
				return render_page(variables, template_names, request)
			return HttpResponseRedirect('')


