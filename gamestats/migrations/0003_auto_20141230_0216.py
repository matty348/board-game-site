# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gamestats', '0002_auto_20141230_0211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='game',
            name='image',
            field=models.CharField(max_length=250, null=True, blank=True),
        ),
    ]
