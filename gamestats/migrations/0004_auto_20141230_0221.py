# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gamestats', '0003_auto_20141230_0216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='play',
            name='play_image',
            field=models.FileField(null=True, upload_to=b'play_images/%Y/%m/%d', blank=True),
        ),
    ]
