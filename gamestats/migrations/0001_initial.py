# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bgg_id', models.IntegerField()),
                ('name', models.CharField(unique=True, max_length=250)),
                ('min_players', models.IntegerField(default=2)),
                ('max_players', models.IntegerField(default=2)),
                ('bgg_playing_time', models.IntegerField(default=2)),
                ('bgg_bayesaverage', models.DecimalField(max_digits=3, decimal_places=2)),
                ('description', models.TextField()),
                ('image', models.CharField(max_length=250)),
                ('weight', models.DecimalField(max_digits=5, decimal_places=4)),
                ('added', models.DateTimeField(default=datetime.datetime.now)),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mechanic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=250)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Play',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('play_image', models.FileField(null=True, upload_to=b'play_images/%Y/%m/%d')),
                ('date', models.DateField(default=datetime.date.today)),
                ('play_time', models.IntegerField()),
                ('num_players', models.IntegerField()),
                ('use_for_stats', models.BooleanField()),
                ('expansions', models.ManyToManyField(related_name=b'Play Expansions', null=True, to='gamestats.Game', blank=True)),
                ('game', models.ForeignKey(to='gamestats.Game')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Playstat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('play', models.ForeignKey(to='gamestats.Play')),
                ('player', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Stat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Value',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='stat',
            name='values',
            field=models.ManyToManyField(to='gamestats.Value', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='playstat',
            name='stat',
            field=models.ForeignKey(to='gamestats.Stat'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='Gamestats',
            field=models.ManyToManyField(related_name=b'Gamestats', null=True, to='gamestats.Stat', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='Playerstats',
            field=models.ManyToManyField(to='gamestats.Stat', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='categories',
            field=models.ManyToManyField(to='gamestats.Category'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='expands',
            field=models.ForeignKey(related_name=b'Expansions', default=None, blank=True, to='gamestats.Game', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='mechanics',
            field=models.ManyToManyField(to='gamestats.Mechanic'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='game',
            name='owners',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
